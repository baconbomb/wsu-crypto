/*
 * Spencer Bacon
 * CS427
 * Proj 1
 * Encryption algorithm
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>
#include "WSU-ENCRYPT.h"
#include "WSU-CRYPT.h"

extern int Round;

int encrypt(){

    FILE *input;
    FILE *output;
    FILE *keyFile;

    input = fopen(PLAINTEXT,  "r");
    output = fopen(CYPHERTEXT, "wb");
    keyFile = fopen(KEYTEXT, "wb");

    if(input == NULL){
        perror("");
        return (EXIT_FAILURE);
    }
    if(output == NULL){
        perror("");
        return (EXIT_FAILURE);
    }

    uint64_t key;    
    
    srand(time(0));                 //seed random number for key
    key = rand();                   //rand outputs 32 bits
    key = key << 32 | rand();       //store 64 bit key

    uint64_t word = 0;

    outputE(keyFile, key);          //write hex key to key.txt

    int amountRead = 0;
    while((amountRead = readInE(input, &word)) == 8){   //read in bits loop - exits if no more bits to read or less then 8 bytes 
        word = doencrypt(word, &key);                   //encrypt 8 byte word
        outputE(output, word);                          //write to cyphertext.txt
    }

    if(amountRead != 0){                        //0 means nothing else to read (ends in 8 bytes block), still need to encrypt partial block 
        word = doencrypt(word, &key);           //encrypt less then 8 bytes word
        outputE(output, word);                  //write to cyphertext.txt
    }

    fclose(input);
    fclose(output);
    fclose(keyFile);

    return(EXIT_SUCCESS);
}

int readInE(FILE* input, uint64_t *word){
    *word = 0x0101010101010101;             //default unprintable ascii
    uint8_t charRead;
    int amountRead = 0;

    for(amountRead; amountRead < 8; amountRead++){      //increment amount of chars read in
        charRead = fgetc(input);                        //get char
        if(feof(input)){                                //EOF
            return amountRead;                          //return < 8 bytes read
        }
        *word = *word << 8;                             //bit shit to store new byte
        *word = *word + charRead;                       //store new byte
    }
    return amountRead;                                  //return amount of chars read
}

void outputE(FILE* output, uint64_t word){
    fprintf(output, "%016lx", word);                    //print 16 hex numbers to output file
}

uint64_t doencrypt(uint64_t word, uint64_t *key){

    uint16_t w[4];
    w[0] = (word >> 48) & 0x0000FFFF;       //break word apart to two bytes each
    w[1] = (word >> 32) & 0x0000FFFF;
    w[2] = (word >> 16) & 0x0000FFFF;
    w[3] = (word) & 0x0000FFFF;

    uint16_t k[4];                          //break key apart to two bytes each
    k[0] = (*key >> 48) & 0x0000FFFF;
    k[1] = (*key >> 32) & 0x0000FFFF;
    k[2] = (*key >> 16) & 0x0000FFFF;
    k[3] = *key & 0x0000FFFF;

    uint16_t R[4];
    for(int i = 0; i < 4; i++){
        R[i] = w[i] ^ k[i];                 //input whitening
    }

    uint16_t F[2];

    for(int i = 0; i < 16; i++){
        FuncFE(&R[0], &R[1], &F[0], &F[1], key);    //call function F for encryption

        uint16_t r[4];

        r[0] = rotateRight(R[2] ^ F[0], 1);         //solve for next round R
        r[1] = rotateLeft(R[3] , 1) ^ F[1];
        r[2] = R[0];
        r[3] = R[1];

        R[0] = r[0];
        R[1] = r[1];
        R[2] = r[2];
        R[3] = r[3];

        Round++; //increment round counter
    } 

    uint16_t y[4];
    y[0] = R[2];        //undo last round
    y[1] = R[3];
    y[2] = R[0];
    y[3] = R[1];

    uint16_t C[4];
    for(int i = 0; i < 4; i++){
        C[i] = y[i] ^ k[i];     //output whitening
    }

    uint64_t finalWord = 0;
    for(int j = 0; j < 4; j++){                 //concatenate word together
        finalWord = finalWord << 16;
        finalWord = finalWord + C[j];
    }

    Round = 0;
    return finalWord;
}


uint8_t FuncKE(uint16_t x, uint64_t *key){      //Function K for encryption

    uint64_t oldkey = *key;
    *key = ((oldkey << 1) | (oldkey >> (64 - 1)));  //circle rotate old key to new key
    
    uint8_t k[8];

    for(int i = 0; i <8 ; i++){
        k[i] = (*key >> i * 8) & 0xFF;          //seperate subkeys to bytes
    }
    return k[x % 8];                            //return the correct byte of the key
}

uint16_t FuncGE(uint16_t *w, uint64_t *key){    //Function G for encryption
    uint8_t g[6];

    g[0] = (*w >> 8) & 0xFF;                    //solve for g
    g[1] = *w & 0xFF;
    g[2] = (Ftable(g[1] ^ FuncKE(4 * Round, key))) ^ g[0];
    g[3] = (Ftable(g[2] ^ FuncKE(4 * Round + 1, key))) ^ g[1];
    g[4] = (Ftable(g[3] ^ FuncKE(4 * Round + 2, key))) ^ g[2];
    g[5] = (Ftable(g[4] ^ FuncKE(4 * Round + 3, key))) ^ g[3];
    
    return concatenate(g[4], g[5]);
}

void FuncFE(uint16_t *R0, uint16_t *R1, uint16_t *F0, uint16_t *F1, uint64_t *key){     //Function F for encryption
    uint16_t T0 = FuncGE(R0, key);  
    uint16_t T1 = FuncGE(R1, key);

    uint8_t k[4];
    k[0] = FuncKE(4 * Round, key);
    k[1] = FuncKE(4 * Round + 1, key);
    k[2] = FuncKE(4 * Round + 2, key);
    k[3] = FuncKE(4 * Round + 3, key);

    *F0 = (T0 + (2 * T1) + concatenate(k[0], k[1])) % 65536;

    *F1 = (2 * T0 + T1 + concatenate(k[2], k[3])) % 65536;
}
