/*
 * Spencer Bacon
 * CS427
 * Proj 1
 * Main function header file
 */

#include <inttypes.h>

#define PLAINTEXT "plaintext.txt"       
#define CYPHERTEXT "cyphertext.txt"
#define KEYTEXT "key.txt"

#define WORDSIZE 16

uint16_t rotateLeft(uint16_t, int);     //circular rotate left

uint16_t rotateRight(uint16_t, int);    //circular rotate right

uint16_t concatenate(uint16_t, uint16_t);   //concatenate two bytes together

uint16_t Ftable(uint8_t);       //look up table
