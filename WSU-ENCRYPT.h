/*
 * Spencer Bacon
 * CS427
 * Proj 1
 * Encryption algorithm header file
 */

#include <stdio.h>
#include <inttypes.h>

int encrypt();      //start encryption process

int readInE(FILE*, uint64_t*);  //read bytes from plaintext

void outputE(FILE*, uint64_t);  //output to cyphertext

uint64_t doencrypt(uint64_t, uint64_t*);    //start encryption algorithm

uint8_t FuncKE(uint16_t, uint64_t*);        //Function K for encryption

uint16_t FuncGE(uint16_t*, uint64_t*);      //Function G for encryption

void FuncFE(uint16_t*, uint16_t*, uint16_t*, uint16_t*, uint64_t*); //Function F fpr encryption
