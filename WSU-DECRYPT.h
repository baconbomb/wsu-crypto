/*
 * Spencer Bacon
 * CS427
 * Proj 1
 * Decryption algorithm header file
 */

#include <stdio.h>
#include <inttypes.h>

int decrypt();  //start decryption process

int readInD(FILE*, uint64_t*);  //read hex from cyphertext

void outputD(FILE*, uint64_t);  //output to plaintext 

uint64_t dodecrypt(uint64_t, uint64_t*);    //do decryption

uint8_t FuncKD(unsigned int, uint64_t*);    //Function K for decryption

uint16_t FuncGD(uint16_t*, uint8_t*);  //Function G for decryption

void FuncFD(uint16_t*, uint16_t*, uint16_t*, uint16_t*, uint64_t*); //Function F for decryption
