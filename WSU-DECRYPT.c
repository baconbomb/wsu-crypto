/*
 * Spencer Bacon
 * CS427
 * Proj 1
 * Decryption algorithm
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <inttypes.h>
#include "WSU-DECRYPT.h"
#include "WSU-CRYPT.h"

extern int Round;

int decrypt(){

    FILE *input;
    FILE *output;
    FILE *keyFile;

    input = fopen(CYPHERTEXT, "rb");
    output = fopen(PLAINTEXT, "w");
    keyFile = fopen(KEYTEXT, "rb");

    if(input == NULL){
        perror("");
        return (EXIT_FAILURE);
    }
    if(output == NULL){
        perror("");
        return (EXIT_FAILURE);
    }
    if(keyFile == NULL){
        perror("");
        return(EXIT_FAILURE);
    }

    uint64_t key;

    readInD(keyFile, &key);     //read in hex key from key.txt

    uint64_t word = 0;

    int amountRead = 0;
    while((amountRead = readInD(input, &word)) == 8){ //read in 64 bit word from cyphertext.txt
        word = dodecrypt(word, &key);   //decrypt word
        outputD(output, word);          //output to plaintext.txt
    }
    
    fclose(input);
    fclose(output);
    fclose(keyFile);

    return(EXIT_SUCCESS);
}

int readInD(FILE* input, uint64_t *word){ //read in 64 bit word from cyphertext
    *word = 0;
    uint8_t charRead;
    int amountRead = 0;

    uint8_t charsRead[17];          //16 hex numbers plus \0
    uint8_t chars[8];               //store 8 bytes

    if(fgets(charsRead, sizeof(charsRead), input) != NULL){ //will always have 8 bytes to read
        int i = 0;
        int j = 0;
        while(charsRead[i] != '\0'){    //loop through string until \0
            int w;
            sscanf(&charsRead[i], "%2x", &w);   //convert string to hex number
            chars[j] = w;           //store number
            i = i + 2;
            j++;
        }
        for(j = 0; j < 8; j++){
            *word = *word << 8;     //concatenate bytes to make 64 bit word
            *word = *word + chars[j];
        }
    return 8;       //should always be 8 bytes
    }

    else {
        return 0;   //read nothing
    }
}

void outputD(FILE* output, uint64_t word){
    char w[8];

    for(int i = 0; i < 8; i++){
        w[i] = (word >> i * 8) & 0xFF;      //bit mask one byte at time
    }

    for(int i = 0; i < 8; i++){
        if(w[7 - i] != 0x01){
            fprintf(output, "%c", w[7 - i]);    //print ascii char to file plaintext.txt
        }
    }
}

uint64_t dodecrypt(uint64_t word, uint64_t *key){ //decrypt cyphertext

    Round = 0;

    uint16_t w[4];
    w[0] = (word >> 48) & 0x0000FFFF;       //break word apart in to two bytes
    w[1] = (word >> 32) & 0x0000FFFF;
    w[2] = (word >> 16) & 0x0000FFFF;
    w[3] = (word) & 0x0000FFFF;
    
    uint16_t k[4];                //break key apart in to two bytes
    k[0] = (*key >> 48) & 0x0000FFFF;
    k[1] = (*key >> 32) & 0x0000FFFF;
    k[2] = (*key >> 16) & 0x0000FFFF;
    k[3] = *key & 0x0000FFFF;

    uint16_t R[4];
    for(int i = 0; i < 4; i++){
        R[i] = w[i] ^ k[i];         //input whitening
    }

    
    uint16_t F[2];

    for(int i = 0; i < 16; i++){
        FuncFD(&R[0], &R[1], &F[0], &F[1], key);    //call F function for decryption

        uint16_t r[4];

        r[0] = rotateLeft(R[2], 1) ^ F[0];      //solve for next round R
        r[1] = rotateRight(R[3] ^ F[1], 1); 
        r[2] = R[0];
        r[3] = R[1];

        R[0] = r[0];
        R[1] = r[1];
        R[2] = r[2];
        R[3] = r[3];

        Round++; //increment round counter????
    }

    uint16_t y[4];
    y[0] = R[2];        //undo last round
    y[1] = R[3];
    y[2] = R[0];
    y[3] = R[1];

    uint16_t C[4];
    for(int i = 0; i < 4; i++){
        C[i] = y[i] ^ k[i];     //output whitening
    }
    
    uint64_t finalWord = 0;
    
    for(int j = 0; j < 4; j++){     //concatenate 64 bit word
        finalWord = finalWord << 16;
        finalWord = finalWord + C[j];
    }

    Round = 0;
    return finalWord;
}


void FuncFD(uint16_t *R0, uint16_t *R1, uint16_t *F0, uint16_t *F1, uint64_t *key){     //Function F for decryption
    
    uint8_t k[12];

    for(int i = 0; i < 12; i++){        //generate all subkeys first
        k[i] = FuncKD(4 * Round + (i % 4), key);
    }

    uint16_t T0 = FuncGD(R0, &k[8]); 
    uint16_t T1 = FuncGD(R1, &k[4]);

    *F0 = (T0 + (2 * T1) + concatenate(k[3], k[2])) % 65536;

    *F1 = (2 * T0 + T1 + concatenate(k[1], k[0])) % 65536;
}


uint16_t FuncGD(uint16_t *w, uint8_t *k){    //Function G for decryption
    uint8_t g[6];

    
    g[0] = (*w >> 8) & 0xFF;
    g[1] = *w & 0xFF;
    g[2] = (Ftable(g[1] ^ k[3])) ^ g[0];
    g[3] = (Ftable(g[2] ^ k[2])) ^ g[1];
    g[4] = (Ftable(g[3] ^ k[1])) ^ g[2];
    g[5] = (Ftable(g[4] ^ k[0])) ^ g[3];

    return concatenate(g[4], g[5]);
}

uint8_t FuncKD(unsigned int x, uint64_t *key){ //Function K for decryption
    uint64_t oldkey = *key;
    
    uint8_t k[8];

    for(int i = 0; i <8 ; i++){
        k[7 - i] = (*key >> i * 8) & 0xFF;      //opposite of encryption, can also be done by reversing the round, 15 to 0
    }

    uint8_t z = k[x % 8];

    *key = ((oldkey >> 1) | (oldkey << (64 - 1)));
    return z;
}
